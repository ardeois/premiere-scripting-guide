========================================
Welcome to the Premiere Scripting Guide!
========================================

.. toctree::
    :maxdepth: 2
    :caption: Chapter 1 - Introduction

    1 - Introduction/index
    1 - Introduction/extendscript-overview
    1 - Introduction/how-to-execute-scripts

.. toctree::
    :maxdepth: 2
    :caption: Chapter 2 - App object

    2 - App object/application
    
.. toctree::
    :maxdepth: 2
    :caption: Chapter 3 - Project object

    3 - Project object/project
    
.. toctree::
    :maxdepth: 2
    :caption: Chapter 4 - Project Item object

    4 - Project Item object/projectItem
    
.. toctree::
    :maxdepth: 2
    :caption: Chapter 5 - Sequence object

    5 - Sequence object/sequence

.. toctree::
    :maxdepth: 2
    :caption: Chapter 6 - Track object

    6 - Track object/track

.. toctree::
    :maxdepth: 2
    :caption: Chapter 7 - Track Item object

    7 - Track Item object/trackitem

.. toctree::
    :maxdepth: 2
    :caption: Chapter 8 - Component object

    8 - Component object/component

.. toctree::
    :maxdepth: 2
    :caption: Chapter 9 - Component Parameter object

    9 - Component Parameter object/componentparam

.. toctree::
    :maxdepth: 2
    :caption: Chapter 10 - Anywhere object

    10 - Anywhere object/anywhere

.. toctree::
    :maxdepth: 2
    :caption: Chapter 11 - Encoder object

    11 - Encoder object/encoder

.. toctree::
    :maxdepth: 2
    :caption: Chapter 12 - Marker object

    12 - Marker object/marker
